import { Injectable } from '@angular/core';

const admin: string = 'admin';
const pass: string = 'pass';


@Injectable()
export class LoginService {
  authenticated = false;

  constructor() { }

  login(username, password) {
    if(username == admin && password == pass) {
      this.authenticated = true;
    } else {
      this.authenticated = false;
    }
  }

  isAuthenticated() {
    return this.authenticated;
  }

}
