import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  @Input() isLoggedIn:boolean =false;
  constructor(private loginservice : LoginService) { }

  ngOnInit() {
    this.updateflag();
  }

  updateflag() {
    this.isLoggedIn = this.loginservice.isAuthenticated();
    console.log("val"+this.isLoggedIn);
  }

}
