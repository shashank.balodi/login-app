import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  error: string = ''

  @Output() isLoggedIn = new EventEmitter();
  
  constructor(private loginservice : LoginService, private router : Router) { }

  ngOnInit() {
  }

  formSubmit(username, password) {
    this.loginservice.login(username, password);
    if(this.loginservice.isAuthenticated()) {
      this.username = '';
      this.password = '';
      this.error = '';
      this.isLoggedIn.emit(true);
      console.log(this.isLoggedIn);
      this.router.navigateByUrl('dashboard');
    } else {
      this.error = "please enter valid username/ password";
      console.log('please go away....');
    }
  }

}
