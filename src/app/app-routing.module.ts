import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';

const routes : Routes = [
    {path:'', redirectTo: '/login', pathMatch: 'full'},
    {path: 'dashboard', component: DashboardComponent },
    {path: 'login', component: LoginComponent },
    {path: 'about', component: AboutComponent},
    {path: 'userDetail', component: UserDetailComponent}
]

@NgModule({
  imports : [
    RouterModule.forRoot(routes), 
    CommonModule
  ],
  exports : [RouterModule]
})

export class AppRoutingModule {
    
 }
